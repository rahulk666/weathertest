# Create your models here.
from datetime import datetime

from django.db import models

CITIES = [{'id': 0, 'name': 'Hoboken, NJ, USA', 'lt': 40.745255, 'ln': -74.034775},
          {'id': 1, 'name': 'Port Hueneme, CA, the US', 'lt': 34.155834, 'ln': -119.202789},
          {'id': 2, 'name': 'Auburn, NY, USA', 'lt': 42.933334, 'ln': -76.566666},
          {'id': 3, 'name': 'Jamestown, NY, the US', 'lt': 42.095554, 'ln': -79.238609},
          {'id': 4, 'name': 'Fulton, MO, USA', 'lt': 38.846668, 'ln': -91.948059},
          {'id': 5, 'name': 'Bedford, OH, the US', 'lt': 41.392502, 'ln': -81.534447},
          {'id': 6, 'name': 'Stuart, FL, USA', 'lt': 27.192223, 'ln': -80.243057},
          {'id': 7, 'name': 'San Angelo, TX, USA', 'lt': 31.442778, 'ln': -100.450279},
          {'id': 8, 'name': 'Woodbridge, NJ, USA', 'lt': 40.560001, 'ln': -74.290001},
          {'id': 9, 'name': 'Vista, CA, USA', 'lt': 33.193611, 'ln': -117.241112}]


class Weather(models.Model):
    city = models.PositiveSmallIntegerField()
    weather_data = models.JSONField()
    timestamp_unix = models.PositiveBigIntegerField()
    timestamp = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.timestamp = datetime.fromtimestamp(self.timestamp_unix)
        super().save(*args, **kwargs)

    class Meta:
        unique_together = ('city', 'timestamp_unix')
        get_latest_by = ('timestamp',)
