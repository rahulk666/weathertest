# Register your models here.
from utils.admin import register
from weather.models import Weather

register(Weather)
