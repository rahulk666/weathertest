# Create your views here.
import itertools
import multiprocessing as mp
from datetime import datetime, timedelta
from multiprocessing import Pool

import numpy as np
import pandas as pd
import requests
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

from weather.models import CITIES, Weather
from weathertest.settings import OWM_API_KEY, OWM_API_BASE_URL

mp.set_start_method('fork')  # To access django settings from spawned processes we should fork main process


def get_hourly_weather(city):
    """
    Calls OWM APIs to fetch hourly data.
    Returns list of dict in format of weather.Weather model
    """
    url = f"{OWM_API_BASE_URL}/data/2.5/onecall?lat={city['lt']}&lon={city['ln']}&appid={OWM_API_KEY}"
    res = requests.get(url)
    assert res.status_code == HTTP_200_OK, f"Invalid response from OWM: {res.status_code}"
    hourly_data = res.json()['hourly']
    ret = []
    for data in hourly_data:
        data['city'] = city['name']
        ret.append({
            'city': city['id'],
            'timestamp': datetime.fromtimestamp(data['dt']),
            'timestamp_unix': data.pop('dt'),
            'weather_data': data
        })
    return ret


# Patch to pass lock into process in pool
# def init_lock(l):
#     global lock
#     lock = l


def nearest(df):
    """
    Process a Dataframe with weather data (sum of mean of weather values) and returns closest match for each city
    """
    ret = []
    for i, row in df.iterrows():
        diff = np.abs(df.drop(i)['sum'] - df['sum'][i])
        idx = diff.argmin()
        min_diff = diff.min()
        ret.append((df.drop(i).iloc[idx]['city'], min_diff))
    return ret


class APIViewSet(viewsets.GenericViewSet):
    @action(detail=False, methods=['get'])
    def collect(self, request):
        try:
            # Assuming all cities will have same time periods on hourly forecast
            latest = Weather.objects.latest().timestamp_unix
        except Weather.DoesNotExist:
            latest = 0
        # Calling multiple APIs simultaneously using multiprocess
        pool = Pool(processes=mp.cpu_count() - 1)
        results = [pool.apply(get_hourly_weather, args=(city,)) for city in CITIES]
        results = itertools.chain.from_iterable(results)
        # Only add new data (dt>latest entry in db)
        objs = [Weather(**dt) for dt in results if dt['timestamp_unix'] > latest]
        Weather.objects.bulk_create(objs)
        return Response({"added_count": len(objs)})

    @action(detail=False, methods=['get'])
    def similar(self, request):
        """
        Algorithm to find the most similar weather forecast:
            1. Find all weather data for given period
            2. Find average of each data points (mean(temp), mean(humidity) etc) for each city
            3. Sum all data points together, ie: mean(temp)+ mean(humidity)+ .....
            4. Compare sums from step 3 with each other to find a closest match

        """

        tdelta = timedelta(hours=5)  # Most recent <tdelta> will be taken for similarity calculation
        now = datetime.utcnow()
        weather_data = Weather.objects.filter(timestamp__range=(now, now + tdelta)).values('weather_data')

        # Prepare dataframe by taking only weather_data json from model
        df = pd.DataFrame(weather_data)
        df = pd.DataFrame(list(df.weather_data)).drop('weather', axis=1)

        # find average of each weather data points (temp, feels_like, etc) for each city
        df = df.groupby('city', as_index=False).mean()

        # Adds all mean values to a single value for comparison
        df['sum'] = df.sum(axis=1)
        df = df[['city', 'sum']]

        # Find closest city to each city in weather data mean
        df[['closest_city', 'diff']] = nearest(df)

        # Calculate similarity %
        df['%match'] = (df['sum'] - df['diff']) / df['sum'] * 100

        # Find closest match by taking highest match%
        closest = df.iloc[df['%match'].argmax()]

        return Response({
            'cities': [closest['city'], closest['closest_city']],
            'match_percentage': f"{round(closest['%match'], 3)}"
        })
